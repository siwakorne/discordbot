const Discord = require('discord.js')
const bot = new Discord.Client()

var timeInterval = null

function setBorn(Boss) {
    if(Boss === 'kzarka') {
      let kzarHours = 7
      let kzarMins = 59
      bot.user.setGame(`จะเริ่มเกิดใน ${kzarHours} ชม ${kzarMins} นาที`)
      if(timeInterval) clearInterval(timeInterval)
      timeInterval = setInterval(() => {
        if(kzarMins <= 0 && kzarHours > 0) {
            kzarHours--
            kzarMins = 60
        } else if(kzarMins === 0 && kzarHours === 0) {
          clearInterval(timeInterval)
          setSpawn('kzarka')
        } else {
          kzarMins--
          bot.user.setGame(`จะเริ่มเกิดใน ${kzarHours} ชม ${kzarMins} นาที`)
        }
      }, 60000)
    }
}

function setSpawn(Boss) {
    if(Boss === 'kzarka') {
      let kzarHours = 3
      let kzarMins = 59
      bot.user.setGame(`เกิดใน ${kzarHours} ชม ${kzarMins} นาที`)
      timeInterval = setInterval(() => {
        if(kzarMins <= 0 && kzarHours > 0) {
          kzarHours--
          kzarMins = 60
        } else if (kzarMins === 0 && kzarHours === 0) {
          kzarHours = 0
          kzarMins = 60
        } else {
          kzarMins--
          bot.user.setGame(`เกิดใน ${kzarHours} ชม ${kzarMins} นาที`)
        }
      }, 60000)
    }
}

function setFight (Boss) {
    if(Boss === 'kzarka') {
      let kzarHours = 0
      let kzarMins = 0
      bot.user.setGame(`กำลังต่อสู้ ${kzarHours} ชม ${kzarMins} นาที`)
      if(timeInterval) clearInterval(timeInterval)
      timeInterval = setInterval(() => {
        if(kzarMins >= 59) {
            kzarHours++
            kzarMins = -1
        }
        kzarMins++
        bot.user.setGame(`กำลังต่อสู้ ${kzarHours} ชม ${kzarMins} นาที`)
      }, 60000)
    }

}

bot.on('ready', () => {
    console.log('Kzarka Ready!')
    bot.user.setStatus('online')
    setBorn('kzarka')
})

bot.on('message', message => {
    if( (message.content.includes("kz") || message.content.includes("ซาก้า")) && (message.content.includes("die") || message.content.includes("ตาย")) ) {
      message.channel.sendMessage('@everyone จากเราไปแล้ว TAT')
      setBorn('kzarka')
    } else if ((message.content.includes("kz") || message.content.includes("ซาก้า"))) {
      message.channel.sendMessage('@everyone ข้ามาแล้ว /^;^/')
      setFight('kzarka')
    }
})

bot.login('NDQ4NzU2NjgwNDE5ODM1OTA0.Deawzg.lyGn4QrDcaF3JqkpNV8QWrHnxuk')